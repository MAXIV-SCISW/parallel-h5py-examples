"""
TODO
"""

import h5py
import numpy as np
import time
import sys

def _create_dataset_nofill(group, name, shape, dtype, chunks=None):
	spaceid = h5py.h5s.create_simple(shape)
        plist = h5py.h5p.create(h5py.h5p.DATASET_CREATE)
        plist.set_fill_time(h5py.h5d.FILL_TIME_NEVER)
        if chunks not in [None,[]] and isinstance(chunks, tuple):
            plist.set_chunk(chunks)
        typeid = h5py.h5t.py_create(dtype)
        datasetid = h5py.h5d.create(group.file.id, (group.name+'/'+name).encode('utf-8'), typeid, spaceid, plist)
        dset = h5py.Dataset(datasetid)
        return dset

n_frames = 2000 # 6000
ch_n = g_n = 4096 # 2800

# prepare local data
v = np.arange(g_n,dtype=np.int32).reshape((1,g_n))
data = np.ones((g_n,g_n),dtype=np.int32)
data *= v
data *= v.transpose()
v = None

f = h5py.File(sys.argv[1], 'w')

t1 = time.time()

dset = f.create_dataset('test', (n_frames, g_n, g_n), dtype=np.int32) # , chunks=(1, ch_n, ch_n))
#dset = _create_dataset_nofill(f['/'],'test', (n_frames, g_n, g_n), dtype=np.int32, chunks=(1, ch_n, ch_n))

t2 = time.time()

for i in np.arange(n_frames):
    data += i
    dset[i] = data

f.close()

t3 = time.time()

wrate = data.dtype.itemsize*g_n**2*n_frames/(t3-t2)/1024**2 # MiB/s
print("np: %d, ctime: %.3f s, wtime: %.3f s, wrate: %.3f MiB/s" % (1, t2-t1, t3-t2, wrate,))
print("an equivalent of: %.1f MegaPixel (%d bits) with rate %.1f Hz" % (g_n**2/(1.*1024**2), data.dtype.itemsize*8, n_frames/(t3-t2),))
if wrate>3000.:
    print("under such conditions, a 128 GiB memory buffer is filled in %.1f s" % (128*1024/wrate,))
