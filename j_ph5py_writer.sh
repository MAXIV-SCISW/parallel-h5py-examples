#!/bin/bash
#
# job time, change for what your job requires
#SBATCH -t 00:10:00
#
# job name
#SBATCH -J ph5py
#
#SBATCH --exclusive
#
# filenames stdout and stderr - customise, include %j
#SBATCH -o r_ph5py_%j.out
#SBATCH -e r_ph5py_%j.err

set -o nounset

# write this script to stdout-file - useful for scripting errors
cat $0

# -------------------------------------------------------------------
# disable mostly messy system and python warnings

# h5py fork warning
export OMPI_MCA_mpi_warn_on_fork=0

# openib on mlx5_1 ethernet interface (gn0, ...)
if [ `hostname` == gn0 ];
then
    # disable openib on the mlx5_1 ethernet interface
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

# numpy future warnings
export PYTHONWARNINGS="ignore"

# -------------------------------------------------------------------

# load the modules required for you program - customise for your program
module purge
module use /mxn/groups/pub/sw/modules/maxiv
module add foss/2018a h5py/2.7.1-Python-2.7.14 Python/2.7.14/maxiv/0.4

module list 

export n_tasks=$1
export filename=$2

# run the program
# customise for your program name and add arguments if required
mpirun --map-by node --bind-to core -n $n_tasks python parallel_writer.py $filename

# Usage: sbatch -N 8 j_ph5py_writer.sh 128 /gpfs/online016m/test/parallell_test.hdf5
