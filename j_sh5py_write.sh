#!/bin/bash
#
# job time, change for what your job requires
#SBATCH -t 00:20:00
#
# job name
#SBATCH -J sh5py
#
#SBATCH --exclusive
#
# filenames stdout and stderr - customise, include %j
#SBATCH -o r_sh5py_%j.out
#SBATCH -e r_sh5py_%j.err

set -o nounset

# write this script to stdout-file - useful for scripting errors
cat $0

# -------------------------------------------------------------------
# disable mostly messy system and python warnings

# h5py fork warning
export OMPI_MCA_mpi_warn_on_fork=0

# openib on mlx5_1 ethernet interface (gn0, ...)
if [ `hostname` == gn0 ];
then
    # disable openib on the mlx5_1 ethernet interface
    export OMPI_MCA_btl_openib_if_exclude=mlx5_1    
fi

# numpy future warnings
export PYTHONWARNINGS="ignore"

# -------------------------------------------------------------------

# load the modules required for you program - customise for your program
module purge
module add Anaconda3/2019.03
module list 

source activate h5py-an19.03
conda list

export filename=$1

# run the program
# customise for your program name and add arguments if required
python serial_writer.py $filename

# Usage: sbatch j_sh5py_write.sh /gpfs/online016m/test/serial_test.hdf5
