# parallel-h5py-examples

## serial HDF5

Show effects of chunking and filling on basic processing examle.

1.  write continuous dataset
2.  write chunked dataset
3.  write chunked dataset without filling

```bash
sbatch j_sh5py_write.sh /gpfs/online016m/test/serial_test.hdf5
```

Use dask-reader to demonstrate effect of chunking on IO-read performance.

```bash
sbatch j_dh5py_reader.sh /gpfs/online016m/test/persistent/serial_test_notchunked.hdf5
```

## parallel HDF5

Show a basic MPI writer example. Demonstrate how it scales with nodes and what is
the perfoermance using Power8 with 2xFDR.

```bash
sbatch -N 1 j_ph5py_writer.sh 8 /gpfs/online016m/test/parallell_test.hdf5
```

```bash
# setup enviromental modules for POWER8
source /maxiv/source_me.sh

# load modules
module add h5py/2.7.0-foss-2017a-Python-2.7.13-HDF5-1.10.1

# run test
mpirun --bind-to core -n 8 python parallel_writer.py /gpfs/online016m/test/parallell_test.hdf5
```
## References

1.  [apdavison/demo.py on GitHub](https://gist.github.com/apdavison/36126ee26067592ee69bf51b57fd3f31)