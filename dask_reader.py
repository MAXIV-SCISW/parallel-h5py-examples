"""

"""

import h5py
import numpy as np
import time
from dask.distributed import Client, LocalCluster
import sys

tasks = 16
frames = 6000
filename = sys.argv[1]

def read(df):
	f = h5py.File(df['filename'], 'r', swmr=True)
	dset = f['/test']
	total = 0
	for i in np.arange(df['rank'],df['frames'],df['tasks']):
		total += dset[i,:,:].sum()
	f.close()
	return total

# create DataFrame
df = [{'rank':i, 'filename':filename, 'frames':frames, 'tasks':tasks} for i in range(tasks)]

cluster = LocalCluster(n_workers=16,processes=True,threads_per_worker=1)

val = None

with Client(cluster) as client:
	A = client.map(read, df)

	t1 = time.time()
	total = client.submit(sum, A)
	val = total.result()
	t2 = time.time()

f = h5py.File(filename, 'r', swmr=True)
dset = f['/test']

rrate = dset.dtype.itemsize*dset.shape[1]*dset.shape[2]*frames/(t2-t1)/1024**2 # MiB/s
print("filename: %s" % (filename,))
print("np: %d, time: %.3f s, rrate: %.3f MiB/s" % (tasks, t2-t1, rrate,))

f.close()
cluster.close()
